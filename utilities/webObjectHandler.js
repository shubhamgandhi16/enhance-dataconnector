/*******************************************************************************
 * Function Description: This whole file to fetch locator from appWebObjects.js and covert in element obj that can be used anywhere into code
 * Created Date : 07/17/2017
 * Created by : Sudip Prasad
 * Input: locator in format "PageName|ObjectName"
 * Output: element (web element instance)
 *********************************************************************************/

let pageObject = require('./../testObjects/appWebObjects.js');

function getWebElement(locator, callback) {
  getByObject(locator, function (by_obj) {
   return callback(element(by_obj));
  });
}

function getByObject(locator, callback) {
  getObjectSelectorDetails(locator, function (obj_json) {
    switch (obj_json.locateStrategy) {
      case 'xpath':
        return callback(by.xpath(obj_json.selector));
        break;
      case 'css':
        return callback(by.css(obj_json.selector));
        break;
      case 'className':
        return callback(by.className(obj_json.selector));
        break;
      case 'tagName':
        return callback(by.tagName(obj_json.selector));
        break;
      case 'partialLinkText':
        return callback(by.partialLinkText(obj_json.selector));
        break;
      case 'linkText':
        return callback(by.linkText(obj_json.selector));
        break;
      case 'name':
        return callback(by.name(obj_json.selector));
        break;
      case 'id':
        return callback(by.id(obj_json.selector));
        break;
    }
  });
}

function getObjectSelectorDetails(locator, callback) {
  return callback(pageObject[locator.split("|")[0]][locator.split("|")[1]]);
};

module.exports = {
  getWebElement: getWebElement,
  getByObject: getByObject
};
