@PE_ED_FU
Feature: To validate Employee Data section in production

  @ED_FU_1
  Scenario: User navigates to Employee Data section after adding a new company-US and uploads a MDT successfully
    Given User navigates to "Client_Collector" application with "username" and "password"
    And User adds a company "Subway" of "UnitedStates" and navigates to the dashboard
    When User initiates "ProductionCampaign" campaign
    And User initiates "EmployeeData" section
    Then User should be navigated to Grid Summary Page
    And User navigates to File upload page from Grid Summary page
    And User selects "Subway_US" company and checks its status
    When User uploads "US_MDT" MDT file in the grid section for "Subway"
    Then File should be uploaded successfully

  @ED_FU_2
  Scenario: User navigates to Employee Data section after adding a new company-PL and checks for L0 Validations
    Given User navigates to "Client_Collector" application with "username" and "password"
    Given User adds a company "KFC" of "Poland" and navigates to the dashboard
    When User initiates "ProductionCampaign" campaign
    And User initiates "EmployeeData" section
    Then User should be navigated to Grid Summary Page
    And User navigates to File upload page from Grid Summary page
    And User selects "KFC_PL" company and checks its status
    When User uploads "PL_DuplicateColumnsFile" file in the grid section for "Subway"
    Then The system should throw an error