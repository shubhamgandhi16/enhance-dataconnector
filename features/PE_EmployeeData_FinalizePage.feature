@PE_ED_FP
Feature: To validate Employee Data section in production

  @ED_FP_1
  Scenario: User navigates to Employee Data section after adding a new company
    Given User navigates to "Client_Collector" application with "username" and "password"
    And User adds a company "Subway" of "UnitedStates" and navigates to the dashboard
    When User initiates "ProductionCampaign" campaign
    And User initiates "EmployeeData" section