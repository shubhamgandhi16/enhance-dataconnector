@PE_LTI
Feature: To validate LTI Plans section in production

  @PE_LTI_1
  Scenario: User navigates to LTI Plans section after adding a new company
    Given User navigates to "Client_Collector" application with "username" and "password"
    And User adds a company "Subway" of "UnitedStates" and navigates to the dashboard
    When User initiates "ProductionCampaign" campaign
    And User initiates "LTIPlans" section