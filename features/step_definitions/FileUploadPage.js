var {defineSupportCode}= require("cucumber");
fileUploadPage= require("../../Pages/FileUploadPage");

defineSupportCode(function ({Given, When, Then}) {

    Given(/^User selects "([^"]*)" company and checks its status/, function (companyName, done) {
        console.log(globalData[companyName]);
        fileUploadPage.selectCompany(globalData[companyName], function(){
            fileUploadPage.checkingStatusOfCompany(function () {
                done();
            });
        });
    });

    When(/^User uploads "([^"]*)" MDT file in the grid section for "([^"]*)"/, function (fileName, done) {
        console.log(globalData[fileName]);
        fileUploadPage.uploadMDTFile(globalData[fileName], function () {
            done();
        });
    });

    When(/^User uploads "([^"]*)" file in the grid section for "([^"]*)"/, function (fileName, done) {
        console.log(globalData[fileName]);
        fileUploadPage.uploadOtherFile(globalData[fileName], function () {
            done();
        });
    });

    Then(/^File should be uploaded successfully/, function (done) {
        keyword.expectVisible(fileUploadPage.thankyouMessage, function () {
            done();
        });
    });

    Then(/^The system should throw an error/, function (done) {
        keyword.expectVisible(fileUploadPage.L0_Error, function () {
            done();
        });
    });
});
