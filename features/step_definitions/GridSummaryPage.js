var {defineSupportCode}= require("cucumber");

Header= require("../../Pages/HeaderPage");
FileUploadPage= require("../../Pages/FileUploadPage");

defineSupportCode(function ({Given, When, Then}) {

    Given(/^User navigates to File upload page from Grid Summary page/, function (done) {
        keyword.expectVisible(Header.fileUploadButton, function () {
            keyword.performClick(Header.fileUploadButton, function() {
                keyword.expectVisible(FileUploadPage.SelectCompany_dropdown, function () {
                    keyword.clickpendolocator(function () {
                        done();
                    });
                });
            });
        });
    });
});
