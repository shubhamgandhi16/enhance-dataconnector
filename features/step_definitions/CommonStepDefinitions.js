var {defineSupportCode} = require("cucumber");
loginPage = require('../../Pages/LoginPage');
dashboardPage = require('../../Pages/HomePage');
qbPage = require('../../Pages/QuestionBank_HomePage');
cmPage = require('../../Pages/CampaignManagement_HomePage');
editCompanyPage = require("../../Pages/EditCompany_Page");
Headers= require("../../Pages/HeaderPage");
CampaignOverviewPage= require("../../Pages/CampaignOverviewPage");
GridSummaryPage= require("../../Pages/GridSummaryPage")

defineSupportCode(function ({Given, Then, When}) {

    Given(/^User navigates to "([^"]*)" application with "([^"]*)" and "([^"]*)"$/, function (url, username, password, done) {
        loginPage.loginEnhance(url, username, password, function () {
            dashboardPage.checkOnTheAgreementAndPage(function () {
                dashboardPage.handleValidationToast(function () {
                    done();
                });
            });
        });
    });

    Then(/^User adds a company "([^"]*)" of "([^"]*)" and navigates to the dashboard$/, function (companyName, countryName, done) {
        keyword.performClick(dashboardPage.gearIcon, function() {
            keyword.performClick(dashboardPage.companiesLink, function() {
                editCompanyPage.deleteAddedCompanies(function() {
                    editCompanyPage.addACompany(globalData[companyName], globalData[countryName], function() {
                        keyword.waitALittle(function () {
                            keyword.performClick(Headers.dashboardLink, function() {
                                dashboardPage.checkOnTheAgreementAndPage(function () {
                                    done();
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    When(/^User initiates "([^"]*)" campaign$/, function (campaignName, done) {
        dashboardPage.selectingACampaign(globalData[campaignName] , function() {
            done();
        });
    });

    When(/^User initiates "([^"]*)" section$/, function (sectionName, done) {
        CampaignOverviewPage.selectingASection(globalData[sectionName] , function() {
            done();
        });
    });

    Then(/^User should be navigated to Grid Summary Page$/, function () {
        keyword.clickpendolocator(function () {
            keyword.expectVisible(GridSummaryPage.gridSummaryTitle, function () {
                keyword.expectVisible(GridSummaryPage.NavigationNext_arrow, function () {
                });
            });
        });
    });

    Then(/^User should be navigated to Form Summary Page$/, function () {

    });

    Then(/^User should be navigated to Multi form Summary Page$/, function () {

    });
});
