@PE_CD
  Feature: To validate Company Data section in production

    @PE_CD_1
    Scenario: User navigates to Company Data section after adding a new company
      Given User navigates to "Client_Collector" application with "username" and "password"
      And User adds a company "Subway" of "UnitedStates" and navigates to the dashboard
      When User initiates "ProductionCampaign" campaign
      And User initiates "CompanyData" section