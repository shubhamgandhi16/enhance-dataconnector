var overviewPageTitle= Css("div[data-id='subheader-title']");
var sectionByName= "(//mercer-card-header//h2[contains(text(),'<<sectionName>>')]/following::button)[1]";
var beginButton= Xpath("//button[contains(., 'Begin')]");

GridSummaryPage= require("./GridSummaryPage");

function selectingASection(name, callback){
    var section= Xpath(sectionByName.replace("<<sectionName>>", name));
    keyword.expectVisible(beginButton, function() {
        keyword.clickpendolocator(function () {
            keyword.performClick(section, function() {
                keyword.expectVisible(GridSummaryPage.NavigationNext_arrow, function () {
                    callback();
                });
            });
        });
    });
}

module.exports ={
    overviewPageTitle: overviewPageTitle,
    sectionByName: sectionByName,
    beginButton: beginButton,

    selectingASection: selectingASection
}
