var nextBtn = Id('ContentPlaceHolder1_PassiveSignInButton');
var emailInput = Id('txtEmail');
var passwordInput = Name('Password');
var enterButton = Css('button[title="Enter"]');

function hittingApplicationURL(url, callback){
    var execEnv = globalData["TestingEnvironment"].trim();
    console.log("Exe Env: " + execEnv)
    console.log("URL: " + url)
    if (url.toUpperCase() === "QUESTION_BANK") {
        URL = globalData.urls.QUESTION_BANK[execEnv];
        userDB = globalData.users.QUESTION_BANK[execEnv];
        browser.get(URL);
        callback();
    }
    else if (url.toUpperCase() === "CLIENT_COLLECTOR") {
        URL = globalData.urls.CLIENT_COLLECTOR[execEnv];
        console.log("UR Inside: " + URL)
        userDB = globalData.users.CLIENT_COLLECTOR[execEnv];
        browser.get(URL);
        callback();
    } else if (url.toUpperCase() === "CAMPAIGN_MANAGEMENT") {
        URL = globalData.urls.CAMPAIGN_MANAGEMENT[execEnv];
        userDB = globalData.users.CAMPAIGN_MANAGEMENT[execEnv];
        browser.get(URL);
        callback();
    } else if (url.toUpperCase() === "CONNECTOR_ADMIN") {
        URL = globalData.urls.CONNECTOR_ADMIN[execEnv];
        userDB = globalData.users.CONNECTOR_ADMIN[execEnv];
        browser.get(URL);
        callback();
    } else {
        console.log("Invlid");
        throw("Inavlid");
        callback();
    }
}

function loginEnhance(url, emailId, password, callback) {
    hittingApplicationURL(url, function() {
        keyword.waitForSometime(function () {
            browser.getTitle().then(function (title) {
                console.log("Title: " + title);
                if (title === "Mercer Account Center") {
                    keyword.performClick(nextBtn, function () {
                        keyword.expectVisible(emailInput, function () {
                            keyword.setText(emailInput, userDB[emailId], function () {
                                keyword.performClick(passwordInput, function () {
                                    keyword.setText(passwordInput, userDB[password], function () {
                                        keyword.expectVisible(enterButton, function () {
                                            keyword.performClick(enterButton, function () {
                                                callback();
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                } else if (title === "Mercer - Enhance - Client Collector") {

                }else if (title === "Mercer - Enhance - Question Bank") {

                } else if (title === "Mercer - Enhance - Campaign Management") {

                } else if (title === "Mercer - Enhance - Connector Admin") {

                }
            });
        });
    });
}

module.exports = {
    nextButton: nextBtn,
    emailInput: emailInput,
    passwordInput: passwordInput,
    enterButton: enterButton,

    loginEnhance: loginEnhance,
    hittingApplicationURL:hittingApplicationURL
};
