var dashboardLink = Xpath("//span[@class='enh-breadcrumb' and contains(.,'Dashboard')]/a");
var summaryPageButton= Xpath("//button[@icon='description']")
var fileUploadButton= Xpath("//button[@icon='file_upload']");
var jobMatchButton= Xpath("//button[@id='grid-job-match-button']");
var validateEnhanceButton= Xpath("//button[@data-id='grid-employee-data-button']");
var finalizeButton= Xpath("//button[@id='grid-aggregate-validation-button']");

module.exports = {
  dashboardLink: dashboardLink,
  summaryPageButton: summaryPageButton,
  fileUploadButton: fileUploadButton,
  jobMatchButton: jobMatchButton,
  validateEnhanceButton: validateEnhanceButton,
  finalizeButton: finalizeButton
};
