var accountsIcon = Xpath("//mercer-icon[@size='xlg'][@icon='person']/..");
var logout = Xpath("//a[contains(text(),'Logout')]");

module.exports = {
    accountsIcon: accountsIcon,
    logoutButton: logout,
};