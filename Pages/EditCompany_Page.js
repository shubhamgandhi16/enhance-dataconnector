var companiesCount = by.xpath("//cc-companies-list//tbody/tr");
var companyTitle = Xpath("//h2[contains(.,'Your Company or Companies')]");
var actionIcons= by.xpath("//mercer-dropdown-label//button");
var actionIcon_firstCompany= Xpath("(//mercer-dropdown-label//button)[1]");
var deleteButton_actionIcon= Xpath("//mercer-dropdown//following::ul/li/a[text()='Delete']");
var expandCompany = Xpath("//mercer-accordion-close");
var companyName_textbox= Id("complex-company-info-company-name-input");
var selectCountryDropdown= Xpath("//*[@for='country-select']/following::select");
var superSectorDropdown= Css("#super-sector-select");
var sectorDropdown= Css("#sector-select");
var addCompanyButton= Css("button[data-id='complex-company-info-add-btn']");
var saveCompanyButton= Xpath("//button[contains(., 'Save Company')]");

var ConfirmationPopup= require("./ComfirmationPopups");

function deleteAddedCompanies(callback){
    var count=0;
    keyword.expectVisible(companyTitle, function (){
        keyword.getElementsCount(companiesCount, function(companiesCount) {
            console.log("Companies: "+companiesCount)
            for(var i=1; i<=companiesCount; i++){
                keyword.performClick(actionIcon_firstCompany, function() {
                    keyword.performClick(deleteButton_actionIcon, function() {
                        keyword.performClick(ConfirmationPopup.deleteCompany_Yes, function() {
                            count++;
                            if (count === companiesCount) {
                                callback();
                            }
                        });
                    });
                });
            }

        });
        /*keyword.expectVisible(companiesCount, function () {
            element.all(actionIcons).then(function (companies) {
                console.log("Number of Companies: " + companies.length);
                companies.forEach(function (actionIcon) {
                    actionIcon.click().then(function () {
                        keyword.performClick(deleteButton_actionIcon, function () {
                            keyword.performClick(ConfirmationPopup.deleteCompany_Yes, function () {
                                count++;
                                console.log(count)
                            });
                        });
                    });
                });
                if (count == companies.length) {
                    console.log("Done!")
                    callback();
                }
            });
        });*/
    });
}

function addACompany(name, country, callback){
    console.log(name);
    console.log(country);
    keyword.performClick(expandCompany, function() {
        keyword.setText(companyName_textbox, name, function() {
            keyword.waitASecond(function () {
                keyword.selectByVisibleText(selectCountryDropdown, country, function () {
                    keyword.selectByVisibleText(superSectorDropdown, globalData.SuperSector, function () {
                        keyword.selectByVisibleText(sectorDropdown, globalData.Sector, function () {
                            keyword.waitASecond(function () {
                                keyword.expectEnabled(addCompanyButton, function() {
                                    keyword.performClick(addCompanyButton, function () {
                                        keyword.performClick(saveCompanyButton, function () {
                                            callback();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

module.exports = {
    companiesCount: companiesCount,
    companyTitle: companyTitle,

    deleteAddedCompanies: deleteAddedCompanies,
    addACompany:addACompany,
}