var campaigns = Xpath("//mercer-card-header");
var enterCampaign = Xpath("//h2[contains(text(),'<<CampaignName>>')]/following::button[1]");
var termsAgreement = Xpath("//mercer-modal-header");
var acceptAgreementLabel = Xpath("//label[@for='agreement-accept-item']");
var acceptButton = Xpath("//button[text()='Accept']");
var okButton_CookieBar = Xpath("//cc-notice-footer//button");
var okButton_CookieHeader = Xpath("//button[normalize-space(text())='Okay, Got it']");
var pendoClass= ClassName("_pendo-backdrop_");
var gearIcon = Xpath("//*[@icon='settings']");
var companiesLink= Xpath("//a[contains(@href, '/account/companies')]");
var campaignByName= "//h2[contains(text(),'<<CampaignName>>')]/following::button[1]";
var validationToastClose= Xpath("//*[@id='async-toast']//mercer-icon[@icon='close']");

CampaignOverviewPage= require("./CampaignOverviewPage");

function checkOnTheAgreementAndPage(callback){
    keyword.waitForSometime(function () {
        keyword.getElementStatus(termsAgreement, function (status) {
            console.log("Status of Terms and Conditions: " + status);
            if (status == true) {
                keyword.scrollToelementPosition(function () {
                    keyword.performClick(acceptAgreementLabel, function () {
                        keyword.performClick(acceptButton, function () {
                            handleCookieBar(function(){
                                console.log("Cookie Bar handled!");
                                callback();
                            });
                        });
                    });
                });
            } else if(status == false){
                handleCookieBar(function () {
                    console.log("Cookie Bar notFound!");
                    callback();
                });
            }
        });
    });
}

function handleCookieBar(callback){
    keyword.waitALittle(function () {
        keyword.getElementStatus(okButton_CookieBar, function (status) {
            console.log("Status of CookieBar: " + status);
            if (status == true) {
                keyword.performClick(okButton_CookieBar, function () {
                    keyword.waitALittle(function () {
                        keyword.getElementStatus(okButton_CookieHeader, function (status) {
                            console.log("Status of CookieHeader: " + status);
                            if (status == true) {
                                keyword.performClick(okButton_CookieHeader, function () {
                                  callback();
                                });
                            }else {
                                callback();
                            }
                        });
                    });
                });
            }else {
                callback();
            }
        });
    });
}

function selectingACampaign(name, callback) {
    var campaign= Xpath(campaignByName.replace("<<CampaignName>>", name));
    //console.log(JSON.stringify(campaign));
    keyword.expectVisible(campaign, function() {
       keyword.performClick(campaign, function() {
           keyword.expectVisible(CampaignOverviewPage.beginButton, function () {
               callback();
           });
       });
    });
}

function handleValidationToast(callback) {
    keyword.getElementStatus(validationToastClose, function (status) {
        console.log("Validation Toast status: "+status);
        if (status == true) {
            keyword.performClick(validationToastClose, function() {
                callback();
            });
        }else{
            callback();
        }
    });
}

module.exports = {
  campaigns: campaigns,
  enterCampaign: enterCampaign,
  acceptAgreementLabel: acceptAgreementLabel,
  acceptButton: acceptButton,
  termsAgreement: termsAgreement,
  pendoClass: pendoClass,
  gearIcon:gearIcon,
  companiesLink: companiesLink,
  campaignByName:campaignByName,
  validationToastClose: validationToastClose,

  checkOnTheAgreementAndPage: checkOnTheAgreementAndPage,
  handleCookieBar: handleCookieBar,
  selectingACampaign:selectingACampaign,
  handleValidationToast:handleValidationToast,
};
