var FileUploadPage_title = Xpath("//cc-grid-upload");
var SelectCompany_dropdown = Xpath("//cc-selector[@data-id='multi-selector-wrapper']");
var thankyouMessage = Xpath("//h4[contains(.,'Your data has been uploaded.')]");
var ReplaceSubmission_label = Xpath("//label[@for='replace-submission']");
var UpdateSubmission_label = Xpath("//label[@for='update-submission']");
var Upload_button = Css("#form-go-upload");
var termsAndAgreement_label = Xpath("//label[@for='agree-with-terms-checkbox']");
var uploadFile = Xpath("//input[@type='file']");
var validationToastCross = Xpath("//h4[text()='Validation Complete']//ancestor::mercer-toast//mercer-icon[@icon='close']");
var uploadButton = Css("button[data-id='enh-upload-button']");
var L0_Error=Xpath("//mercer-icon[@icon='error']");

function selectCompany(name, callback) {
    keyword.selectByVisibleText(SelectCompany_dropdown, name, function () {
        callback();
    });
}

function checkingStatusOfCompany(callback) {
    keyword.getElementStatus(thankyouMessage, function (status) {
        console.log("Upload status: " + status);
        if (status == true) {
            keyword.performClick(ReplaceSubmission_label, function () {
                keyword.performClick(Upload_button, function () {
                    keyword.expectVisible(termsAndAgreement_label, function () {
                        keyword.performClick(termsAndAgreement_label, function () {
                            callback();
                        });
                    });
                });
            });
        } else {
            keyword.performClick(termsAndAgreement_label, function () {
                callback();
            });
        }
    });
}

function uploadMDTFile(fileName, callback) {
    keyword.expectVisible(uploadButton, function () {
        console.log("Upload Found!");
        keyword.setElementValue(uploadFile, fileName, function () {
            keyword.expectVisible(validationToastCross, function () {
                keyword.performClick(validationToastCross, function () {
                    callback();
                });
            });
        });
    });
}

function uploadOtherFile(fileName, callback) {
    keyword.expectVisible(uploadButton, function () {
        console.log("Upload Found!");
        keyword.setElementValue(uploadFile, fileName, function () {
            browser.sleep(3000);
            keyword.waitALittle(function() {
                callback();
            });
        });
    });
}

module.exports = {
    FileUploadPage_title: FileUploadPage_title,
    thankyouMessage: thankyouMessage,
    ReplaceSubmission_label: ReplaceSubmission_label,
    UpdateSubmission_label: UpdateSubmission_label,
    Upload_button: Upload_button,
    termsAndAgreement_label: termsAndAgreement_label,
    //uploadFile:uploadFile,
    validationToastCross: validationToastCross,
    SelectCompany_dropdown: SelectCompany_dropdown,
    L0_Error:L0_Error,

    selectCompany: selectCompany,
    checkingStatusOfCompany: checkingStatusOfCompany,
    uploadMDTFile: uploadMDTFile,
    uploadOtherFile:uploadOtherFile,

};