var gridSummaryTitle = Css("cc-grid-summary");
var NavigationNext_arrow= Css("mercer-icon[icon='navigate_next']");

module.exports= {
  gridSummaryTitle:gridSummaryTitle,
  NavigationNext_arrow:NavigationNext_arrow,
}
