module.exports = {
  TestingEnvironment: process.env.TEST_ENV || "AWS_DEV",
  // isApplicationAngular: true,
  veryShortWait: 1500,
  shortWait: 4000,
  longWait: 7000,
  veryLongWait: 30000,
  rootElement: '.ctbm-app',

  appURL: {
    QA: 'https://v218-dal-qa-merceros.mercer.com:10481',
    DEV: 'https://enhance.us-east-1.dev.awsapp.mercer.com/clientcollector',
    PROD: 'xxxxxxx'
  },

    urls: {
    CLIENT_COLLECTOR: {
        QA: 'https://v218-dal-qa-merceros.mercer.com:10462',
        UAT: 'https://dal-uat-enhance.mercer.com/clientcollector',
        DEV: 'xxxxxxxxxx',
        AWS_DEV: 'https://enhance-devint.us-east-1.dev.awsapp.mercer.com/clientcollector',
        LOCAL: 'http://localhost:4203',
        AWS_Staging: "https://enhance.us-east-1.stage.awsapp.mercer.com/clientcollector",
      },
    },
    QUESTION_BANK: {
        QA: 'https://v218-dal-qa-merceros.mercer.com:10458',
        UAT: 'https://dal-uat-enhance-operations.mercer.com/questionbank',
        DEV: 'xxxxxxxxxx',
        AWS_DEV:'https://enhance-operations.us-east-1.dev.awsadmin.mercer.com/questionbank',
        LOCAL: 'http://localhost:3204',
        AWS_Staging :"https://enhance-operations.us-east-1.stage.awsadmin.mercer.com/questionbank",
    },
    CAMPAIGN_MANAGEMENT: {
        QA: 'https://v218-dal-qa-merceros.mercer.com:10460',
        UAT: 'https://dal-uat-enhance-operations.mercer.com/campaignmanager',
        DEV: 'xxxxxxxxxx',
        AWS_DEV:'https://enhance-operations.us-east-1.dev.awsadmin.mercer.com/campaignmanager',
        LOCAL: 'http://localhost:3205',
        AWS_Staging :"https://enhance-operations.us-east-1.stage.awsadmin.mercer.com/campaignmanager",
    },
    CONNECTOR_ADMIN:{
        QA: 'https://v218-dal-qa-merceros.mercer.com:10483',
        UAT: 'https://dal-uat-enhance-operations.mercer.com/connectoradmin/',
        DEV: 'xxxxxxxxxx',
        AWS_DEV:'https://enhance-operations.us-east-1.dev.awsadmin.mercer.com/connectoradmin',
        LOCAL: 'http://localhost:3207',
        AWS_Staging :"https://enhance-operations.us-east-1.stage.awsadmin.mercer.com/connectoradmin",
    },

    users: {
        CLIENT_COLLECTOR: {
            QA: {
                username: 'auto345.shubham.gandhi@gisqa.mercer.com',
                password: 'welcome3#'
            },
            AWS_DEV: {
                username: 'autoQA10.santhosh.manikandan@gisqa.mercer.com',
                password: 'welcome2#',
            },
            AWS_Staging:{
                username: 'autoQA10.santhosh.manikandan@gisqa.mercer.com',
                password: 'welcome2#',
            },
        },
    },

    testData: {
    jobDetails: {
      jobFamilyName: 'Engineering and Construction',
      jobSubFamilyName: 'Contract Administration',
      jobCurrency: '£',
      taggedJobRecord: "div[class^='showondesk holderdiv']",
      jobSearchKeyword: 'Analyst',
      jobSubFamilyList: ['PROJECT CONTROLS', 'COST ESTIMATING', 'PROJECT MANAGEMENT', 'SUPPLY & LOGISTICS', 'CONTRACT ADMINISTRATION', 'DOCUMENT CONTROL', 'HEALTH & SAFETY'],
    },
    CompanyData: {
      CompanyData_text:"",
    }
  },
  webMailURL: 'http://mail_mercer.com/owa/5014/',
  ProductionCampaign:'DevInt_CheckAfterSubmission_Mail',
  CompanyData:'Company Data',
  EmployeeData:'Employee Data',
  CompensationPolicies:'Compensation Policies and Practices',
  LTIPlans:'Long Term Incentive Plans',
  RetirementBenefits:'Retirement Benefits',
  InsuranceMedicalBenefits:'Insurance/Medical Benefits',
  CompanyCars:'Company Cars',
  OtherBenefits:'Other Benefits',

  KFC:'KFC',
  KFC_PL:'KFC (PL)',
  Poland:'Poland',
  Poland_Code:'PL',

  Dominos:'Dominos',
  Dominos_ES:'Dominos (ES)',
  Spain:'Spain',
  Spain_Code:'ES',

  UnitedStates:'United States',
  US_Code:'US',
  Subway_US:'Subway (US)',
  Subway:'Subway',

  SuperSector:'Banking/Financial Services',
  Sector:'Consumer Finance & Retail Banking',

  US_MDT:__dirname+'\\2018DataCollection_MDT_US.xlsx',
  PL_DuplicateColumnsFile:__dirname+'\\2018DataCollection_PL_DuplicateColumns.xlsx'
};
